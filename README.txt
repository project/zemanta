Module: Zemanta

Description
===========
Blogging tool, that saves you time, 
brings more traffic and makes your posts beautiful.

Have your browser understand what you are blogging
about and suggest pictures, links, articles and tags 
to make your posts more vibrant. 
While you are writing your post, Zemanta analyzes the 
text and recommends additional 
content you can use to spice it up.

This module integrates Zemanta experience into Drupal workflow.
After install you will have to change 
the input format to allow some additional tags.

If you have any questions regarding the service please use 
http://getsatisfaction.com/zemanta our forum
or support@zemanta.com.


Installation
============
* Copy the 'zemanta' module directory in to your Drupal
sites/all/modules directory as usual.
* Enable module on Site building -> Module or 
/admin/build/modules and enable Zemanta module
* Go to Zemanta settings page Site configuration -> Zemanta or
admin/settings/zemanta and select on which node types you want
to use Zemanta
* On settings page check if your input filters allow tags that 
Zemanta needs and add them
* Go to block settings page Site building -> Blocks or
/admin/build/block and add Zemanta block to right or left sidebar